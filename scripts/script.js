const main = document.getElementById('main')
function decimalAjust(number){
    let factor = Math.pow(10, 2)
    let tempNumber = number * factor
    let roundTempNumber = Math.round(tempNumber)
    return roundTempNumber / factor
}
class Mesh {
    constructor(code, spacingX, spacingY, diameter){
        this.code = code
        this.spacingX = spacingX
        this.spacingY = spacingY
        this.diameter = diameter
        this.areaX = decimalAjust((Math.pow((diameter / 10), 2) / 4 * Math.PI) / (spacingX / 100))
        this.areaY = decimalAjust((Math.pow((diameter / 10), 2) / 4 * Math.PI) / (spacingY / 100))
    }
}
class SteelMesh extends Mesh {
    constructor(code, spacingX, spacingY, diameter){
        super(code, spacingX, spacingY, diameter)
        this.fy = 600
        this.resistenceX = this.areaX * this.fy * 10
        this.resistenceY = this.areaY * this.fy * 10
    }
}
class FRPMesh extends Mesh {
    constructor(code, spacingX, spacingY, diameter, equivalent){
        super(code, spacingX, spacingY, diameter)
        this.fp = 1000
        this.resistenceX = this.areaX * this.fp * 10
        this.resistenceY = this.areaY * this.fp * 10
        this.equivalent = equivalent
    }
}
const standardPanel = [245, 600]

function linearMeters(mesh, area){
    let meters = (((standardPanel[0] / mesh.spacingX) * standardPanel[1]) + ((standardPanel[1] / mesh.spacingY) * standardPanel[0])) / 100
    let totalPanels = Math.ceil(area / (standardPanel[0] / 100 * standardPanel[1] / 100)) 
    let totalMeters = totalPanels * meters
    return totalMeters
}
function getEquivalent(){
    let output = {}
    let userSelection = document.getElementById('mesh').value
    let area = document.getElementById('areaInput').value
    if(area === ''){
        area = standardPanel[0] / 100 * standardPanel[1] / 100
    }
    for(let item in telasFRP){
        
        if(telasFRP[item].equivalent === userSelection){
            output.code = telasFRP[item].code
            output.diameter = telasFRP[item].diameter
            output.spacing = telasFRP[item].spacingX
            output.area = telasFRP[item].areaX
            output.resistence = telasFRP[item].resistenceX
            output.numberOfPanels = Math.ceil(area / (standardPanel[0] / 100 * standardPanel[1] / 100))
            output.linearMeters =  decimalAjust(linearMeters(telasFRP[item], area))
        }
    }

    return output
}
// btn convert
const btnConvert = document.getElementById('convert')
btnConvert.addEventListener('click', () => {
    divResults.innerHTML = ''
    renderResults()})
// btn convert    

// criando a div que vai receber os resultados
    let divResults = document.createElement('div')
    divResults.classList.add("divResults")
    divResults.id = "divResults"
// criando a div que vai receber os resultados
function renderResults(){
        let result = getEquivalent()
        let title = document.createElement('h2')
        console.log(result)
        title.innerText = `Tela de FRP ideal é a ${result.code}`
        title.classList.add("properties")
        title.id = "titleResult"
        divResults.appendChild(title)
        for(let item in result){
            let Prop_item = document.createElement('span')
            Prop_item.id = `${item}`
            Prop_item.classList.add('properties')
            Prop_item.classList.add(`${item}`)
            Prop_item.innerText = result[item]
            divResults.appendChild(Prop_item)
        }
        main.appendChild(divResults)
        
    }