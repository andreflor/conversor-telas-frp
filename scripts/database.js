// telas de Aço
let Q47 = new SteelMesh("Q47", 15, 15, 3)
let Q61 = new SteelMesh('Q61', 15, 15, 3.4)
let Q75 = new SteelMesh("Q75", 15, 15, 3.8)
let Q92 = new SteelMesh('Q92', 15, 15, 4.2)
let Q113 = new SteelMesh("Q113", 10, 10, 3.8)
let Q138 = new SteelMesh('Q138', 10, 10, 4.2)
let Q159 = new SteelMesh('Q159', 10, 10, 4.5)
let Q196 = new SteelMesh('Q196', 10, 10, 5)
let Q246 = new SteelMesh('Q246', 10, 10, 5.6)
let Q283 = new SteelMesh('Q283', 10, 10, 6)
let Q335 = new SteelMesh('Q335', 15, 15, 8)
let Q396 = new SteelMesh('Q396', 10, 10, 7.1)
let Q503 = new SteelMesh('Q503', 10, 10, 8)
let Q636 = new SteelMesh('Q636', 10, 10, 9)
let Q785 = new SteelMesh('Q785', 10, 10, 10)
// telas de Aço
// telas de FRP
let S30 = new FRPMesh('S30', 16.5, 16.5, 2.5, 'Q47')
let S37 = new FRPMesh('S37', 13.2, 13.2, 2.5, 'Q61')
let S50 = new FRPMesh('S50', 9.9, 9.9, 2.5, "Q75")
let S63 = new FRPMesh('S63', 19.8, 19.8, 4, 'Q92')
let S71 = new FRPMesh('S71', 9.9, 9.9, 3, 'Q113')
let S95 = new FRPMesh('S95', 13.2, 13.2, 4, 'Q138')
let S99 = new FRPMesh('S99', 19.8, 19.8, 5, 'Q159')
let S119 = new FRPMesh('S119', 16.5, 16.5, 5, 'Q196')
let S149 = new FRPMesh('S149', 13.2, 13.2, 5, 'Q246')
let S171 = new FRPMesh('S171', 16.5, 16.5, 6, 'Q283')
let S214 = new FRPMesh('S214', 13.2, 13.2, 6, 'Q335')
let S254 = new FRPMesh('S254', 19.8, 19.8, 8, 'Q396')
let S305 = new FRPMesh('S305', 16.5, 16.5, 8, 'Q503')
let S397 = new FRPMesh('S397', 19.8, 19.8, 10, 'Q636')
let S476 = new FRPMesh('S476', 16.5, 16.5, 10, 'Q785')
// telas de FRP
let telasAco = {
    Q47: Q47,
    Q61: Q61,
    Q75: Q75,
    Q113: Q113,
    Q138: Q138,
    Q159: Q159,
    Q196: Q196,
    Q246: Q246,
    Q283: Q283,
    Q335: Q335,
    Q396: Q396,
    Q503: Q503,
    Q636: Q636,
    Q785: Q785
}
let telasFRP = {
    S30: S30,
    S37: S37,
    S50: S50,
    S63: S63,
    S71: S71,
    S95: S95,
    S99: S99,
    S119: S119,
    S149: S149,
    S171: S171,
    S214: S214,
    S254: S254,
    S305: S305,
    S397: S397,
    S476: S476
}