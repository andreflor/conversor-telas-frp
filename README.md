Coversor de telas eletrosoldadas em aço para telas em FRP
Instruções de uso:
1- selecione a tela em aço
2- Coloque a quantidade de metros quadrados vai utilizar 
3- clique no botão converter
4- Os resultados vão aparecer em quantidade de paineis de 2,45m por 6m. 

Para fazer a conversão acesse o seguinte link
https://andreflor.gitlab.io/conversor-telas-frp

Desenvolvido em conjunto com a HAIZER GROUP
